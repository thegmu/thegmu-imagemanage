|

Makefile
--------

**Tools**:

#. **autopep8**: pep8 code beautifier
#. **pylint**: coding standards
#. **pytest**: test source
#. **readthedocs.org**: public documentation using sphinx
#. **sphinx**: html documentation
#. **tox**: test the source as installed package
#. **twine**: deploy the package to pypi.org, test.pypi.org
#. **Makefile**: run the tools


**Configuration files**:

#. **.gitignore**: ignore pylint, pytest, tox and build files as well .settings, .project, and .pydevproject directories from Eclipse.
#. **.pylintrc**: The GMU specific PEP8 suppression.

----

Makefile Commands
=================

make <command>

_default:
 Same as help.

backup-docs:
 Create a temp directory, 'docs.tmp.XXX', using mktemp and copy the docs directory to it.

clean:
 Removes Python compiled files, pytest files, and tox test files. 
 See clean-pyc and clean-tox.

clean-dist:
 Removes Python packaging files.

clean-docs:
 Removes sphinx documentation build files. Configuration files are not removed. 

clean-pyc:
 Removes Python compiled files and pytest files. 

clean-tox: 
 Removes tox test files. 

destroy-docs
 Removes all sphinx config and manually edited document files as well as all generated files.
 See clean-docs.
 See backup-docs.

dist:
 Creates source and binary Python packages suitable for PyPi. 

docs:
 Build the the HTML documentation files in docs/_build.

help:
 Displays this file.

init:
 #. Install Python tools used by this Makefile.
 #. Run project-init, see project-init.

pep8:
    Run ``autopep8`` and update all the project and test files in place with white space changes.

project-init:
 #. setup.py: NAME, AUTHOR, AUTHOR_EMAIL, URL, SCRIPTS all updated.
 #. test/sample_test.py: import of project name updated.
 #. tox.ini: envlist updated

publish:
 #. Publish the package to production 'pypi.org'.
 #. User name and password prompt are given.

publish-test:
 Publish the package to test 'test-pypi.org'.
 User name and password prompt are given.

pylint:
 Run ``pylint`` and output results. No other action is taken. See ``pep8`` option to fix white space problems.

requirements:
 Python 'pip' packages for the tools.

test:
 Run the tests from source using pytest.

test-dist:
 Run the tests from virtual envinorments using tox. Builds the package and then run the test as packages in temporary Python virtualenv environments.

upgrade:
 Upgrade Python 'pip' packages for the tools. 

----

    The reasonable person adapts themself to the world; the unreasonable one persists in trying to adapt the world to themself.  Therefore all progress depends on the unreasonable person. --George Bernard Shaw

**The End**
