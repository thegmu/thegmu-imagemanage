
Almost There Software
---------------------

https://www.thegmu.com/

:Authors: Mybrid Wonderful, Gregg Yearwood
:Date: 12/015/2020
:Support: mybrid@thegmu.com
:Version: 1.0.0

-----

.. image:: https://www.thegmu.com/jaz/static/img/birdie_logo_64x96.png
     

-----

Almost There Introduction
=========================

The objective of the Almost There initiative is to create jobs for people
being displaced by automation. Almost There workers are like plumbers or
auto mechanics who are trades people who have trade training and can be 
self taught. 

Almost There Projects are intended to open the development of software 
to a larger audience than just software developers. The idea of being
almost there is that any user can tweak a software program just a little. 

The tasks of software development that require skills outside the trade
worker are the graphical user interface (GUI) and data management. 
Therefore Almost There software has a texting interface and no GUI. Furthermore
data manipulation is at the simplest level designed to be no more 
complicated than manipulating ingredients for cooking.

-----

Almost There Inspiration One
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One inspiration for Almost There software came from when I read an article
where people who grew up texting could type faster on a phone than 
people using a keyboard. I thought to myself why not take advantage of that 
skill and give users a texting interface to running software? 

There is no GUI in Almost There software because texting the computer itself
is the interface. 

-----

Almost There Inspiration Two
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Software companies want to make the most money possible. This means they 
design software to people with the least computing skills possible. This 
means there is a gap between what's possible and what's available. That gap
comes from GUI's that must be the simplest possible to attract to the most buyers
and this simplicity eliminating complex features due to feature complexity.

Almost There projects fill the gap between simple user interfaces and
and missing complex features.

-----

Almost There Software Mechanic
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We have all the experience of buying clothes like say jeans and the
the sizes available in those jeans are not quite right. We just learn to live
with the misfit. However, sometimes we'll keep looking
for a new jean manufacturer or perhaps get them tailor made. 

Almost There workers are customizers. Think of the workers as artisans. 
You know how we have Art Fairs every summer outdoors, the Art and Wine Festivals? 

Similarly you could have Almost There Fairs. We set up booths and people take
there phones in and have them software customized, Why not? Almost There software
then is not about getting rich, but just earning a day-to-day living providing
personal service much the same way a plumber or an auto mechanic does. 

There is not such personal software market today. The Almost There project
mission is to create that market and hopefully millions of jobs in the 
process.  
 
-----

Almost There Details
====================

Almost There projects have the following details:

#. Open Source code
#. 100% Modifiable
#. Text interface 
#. Text source
#. Do-it-yourself, or Maker design (DIY, Maker)

Almost There software has no web based or graphical interface because it is
not considered a finished product. Also the goal is to get the public engaged
into making slight changes to software such as naming things.

If any Almost There project is worthy of having a graphical interface due to
the value of that project then the Almost There project should be sold or
licensed to the company putting the UI on it. 


-----

Almost There Community
~~~~~~~~~~~~~~~~~~~~~~

Computing is hard. If you are considering working on an Almost There
software project just realize how hard it will be. First off most of 
the online documentation for software is written by developers for developers. 
You'll be wanting to create a blog with all your experience for not only
yourself for others. 

You will struggle going alone. You'll want to search out help beyond just
what you can find online. If you are lucky you'll have access to a developer
like me. 

Installation and configuration will be the big hurdles. Modifying the software 
is designed to be a mechanics work, trade work. However, installing Linux,
installing Python, installing Imagemagick, and all other kinds of software
is hard. When you run into roadblocks don't give up. Ask for help. 

Community can be the job. You should seriously consider not only becoming
an Almost There mechanic for making money, but a content creator as well. 

Learn how to make money on Youtube, Twitch, and blogs using advertising 
revenue. 

The Almost There market will require far more community than the current
software development community because of skill levels. Don't be afraid or
ashamed of that skill level difference, use it to your advantage and 
treat the community as a job as much as the job itself.

-----

Almost There Future
~~~~~~~~~~~~~~~~~~~

Almost There software is just one piece of future governing called
Irreni World Scale. Irreni relies on self-governing. Thomas Jefferson said, 
"The government that governs least governs best because the people govern 
themselves." Jefferson never expanded on what that meant. 

Irreni has a world plan for self-governing and a big part of that is 
understanding that governing yourself means doing for yourself by
becoming a Maker. Long before Capitalism sold us finished consumer products
for everything we made our own clothes. Every home had a sewing machine. 

Almost There software enables you to do for yourself. Sure, in the 
beginning Almost There software capability will be limited because
most software today is consumer software. But as the Almost There market
grows then so will the formal software development change direction to 
produce materials and parts and not just finished goods. This will enable
people with a mechanics skill in software to produce every larger arrays 
of finished software goods themselves. 

Eventually Almost There software will allow 3D printing and Almost There
hardware to expand. Once we start down a social path of becoming our own
Makers for food, clothing, shelter and software then we will rely less
on governing to do so. Relying less on governing is us governing ourselves.

-----

The End
=======
