Welcome to The GMU Image Manage
===============================

.. image:: ../birdie_logo_64x96.png

:mod:`thegmu_imagemanage` is used to manage a directory images using various bulk and indivdiual commands. The bulk commands are listed one per line on a command file. The individual file commands are listed per file inside a spreadsheet containing an image thumbnail and file details. 

- Documentation: http://thegmu-imagemanage.readthedocs.org/
- Source Code: https://bitbucket.org/thegmu/thegmu-imagemanage
- Download: https://pypi.python.org/pypi/thegmu-imagemanage

Please feel free to ask questions via email:
(mybrid@thegmu.com)

.. include: MAKE
.. include: ALMOSTTHERESOFTWARE

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   README
   MAKE
   thegmu_imagemanage

.. toctree::
   :caption: Related Documents:

   ALMOSTTHERESOFTWARE


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
