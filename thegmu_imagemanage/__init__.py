# -*- coding: utf-8 -*-
"""
The GMU requires this version so as to be accessible by both the M
ake program as well as internally within the application.
"""

__version__ = "1.0.0"
